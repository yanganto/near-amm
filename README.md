# near-amm

A demo project for NEAR AMM contract

In this contract, user can swap balance, which should be less than 5% of the pool.
When the ratio is skewed (more than 10% different), the fee will be 5%, else the fee is
constant 1 of token B or the constant ratio * 1 of token A.
