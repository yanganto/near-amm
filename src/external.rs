pub use near_sdk::{env, ext_contract, log, AccountId, Balance, PromiseResult};

pub const TGAS: u64 = 1_000_000_000_000;
pub const NO_DEPOSIT: u128 = 0;
pub const XCC_SUCCESS: u64 = 1;

#[ext_contract(this_contract)]
trait Callbacks {
    fn create_wallet_callback(&mut self);
    fn query_name_callback(&mut self);
}

#[ext_contract(token_a)]
trait TokenA {
    /// create wallet
    #[payable]
    fn new(&mut self) -> String;

    #[payable]
    fn get_name(&mut self) -> String;

    fn get_balance(&mut self, accont: &AccountId) -> Balance;

    fn transfer(&mut self, from: &AccountId, to: &AccountId, amount: Balance);
}

#[ext_contract(token_b)]
trait TokenB {
    /// create wallet
    #[payable]
    fn new(&mut self) -> String;

    #[payable]
    fn get_name(&mut self) -> String;

    fn get_balance(&mut self, accont: &AccountId) -> Balance;

    fn transfer(&mut self, from: &AccountId, to: &AccountId, amount: Balance);
}

pub fn did_promise_succeed() -> bool {
    if env::promise_results_count() != 1 {
        log!("Expected a result on the callback");
        return false;
    }

    match env::promise_result(0) {
        PromiseResult::Successful(_) => true,
        _ => false,
    }
}
