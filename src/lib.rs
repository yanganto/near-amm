//! NEAR AMM contract
//! In this contract, user can swap balance, which should be less than 5% of the pool
//! When the ratio is skewed (more than 10% different), the fee will be 5%, else the fee is
//! constant 1 of token B or the constant ratio * 1 of token A.
use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::{
    env, log, near_bindgen, AccountId, Balance, Gas, PanicOnDefault, Promise, PromiseResult,
};

pub mod external;
pub use crate::external::*;

#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize, PanicOnDefault)]
pub struct Contract {
    token_a_name: Option<String>,
    token_a_wallet: Option<AccountId>,
    token_a_addr: Option<AccountId>,
    token_a_balance: Balance,

    token_b_name: Option<String>,
    token_b_wallet: Option<AccountId>,
    token_b_addr: Option<AccountId>,
    token_b_balance: Balance,

    ratio: usize,
}

#[near_bindgen]
impl Contract {
    /// New a contract with ratio
    #[init]
    #[private]
    pub fn new(ratio: usize) -> Self {
        assert!(!env::state_exists(), "Already initialized");
        Self {
            token_a_name: None,
            token_a_wallet: None,
            token_a_addr: None,
            token_a_balance: 0,
            token_b_name: None,
            token_b_wallet: None,
            token_b_addr: None,
            token_b_balance: 0,
            ratio,
        }
    }

    /// Initialization method:
    /// Input is the address of the contract owner and the addresses of two tokens (hereinafter token A and token B).
    /// The method requests and stores the metadata of tokens (name)
    pub fn initialization(
        &mut self,
        owner: AccountId,
        token_a_addr: AccountId,
        token_b_addr: AccountId,
    ) -> Promise {
        self.token_a_addr = Some(token_a_addr);
        self.token_b_addr = Some(token_b_addr);

        let token_a_name_p = token_a::ext(self.token_a_addr.as_ref().unwrap().clone())
            .with_static_gas(Gas(5 * TGAS))
            .get_name()
            .then(
                Self::ext(env::current_account_id())
                    .with_static_gas(Gas(5 * TGAS))
                    .query_name_callback("A"),
            );
        let token_a_wallet = token_a::ext(self.token_a_addr.as_ref().unwrap().clone())
            .with_static_gas(Gas(5 * TGAS))
            .new()
            .then(
                Self::ext(env::current_account_id())
                    .with_static_gas(Gas(5 * TGAS))
                    .create_wallet_callback("A"),
            );
        let token_b_name_p = token_b::ext(self.token_b_addr.as_ref().unwrap().clone())
            .with_static_gas(Gas(5 * TGAS))
            .get_name()
            .then(
                Self::ext(env::current_account_id())
                    .with_static_gas(Gas(5 * TGAS))
                    .query_name_callback("B"),
            );
        let token_b_wallet = token_b::ext(self.token_b_addr.as_ref().unwrap().clone())
            .with_static_gas(Gas(5 * TGAS))
            .new()
            .then(
                Self::ext(env::current_account_id())
                    .with_static_gas(Gas(5 * TGAS))
                    .create_wallet_callback("B"),
            );
        Promise::new(owner)
            .and(token_a_name_p)
            .and(token_b_name_p)
            .and(token_a_wallet)
            .and(token_b_wallet)
    }

    /// The method for getting information about the contract (ratio of tokens A and B, namely A/B)
    pub fn get_ratio(&self) -> usize {
        self.ratio
    }

    /// The method to check contract initialized or not
    pub fn is_initialized(&self) -> bool {
        self.token_a_name.is_some()
            && self.token_a_wallet.is_some()
            && self.token_b_name.is_some()
            && self.token_b_wallet.is_some()
    }

    /// The transfer fees
    /// If the ratio in 10% different the fee will be 1,
    /// else will be 5% of the amount of transfer
    pub fn fee(&self, token: &str, amount: &Balance) -> Balance {
        let current_ratio = if self.token_b_balance != 0 {
            self.token_a_balance as f32 / self.token_b_balance as f32
        } else {
            f32::MAX
        };
        match token {
            "A" => {
                if (current_ratio - self.ratio as f32) < 0.9 {
                    // current ratio is skewed, it will 5% fee to prevent it skew more
                    amount / 20
                } else {
                    self.ratio as Balance
                }
            }
            "B" => {
                if (current_ratio - self.ratio as f32) > 1.1 {
                    // current ratio is skewed, it will 5% fee to prevent it skew more
                    amount / 20
                } else {
                    1
                }
            }
            _ => panic!("undefined token"),
        }
    }

    /// Add liquidity
    pub fn add_liquidity(&mut self, token: &str, amount: Balance) {
        if !self.is_initialized() {
            log!("Not initialized");
            return;
        }
        match token {
            "A" => {
                token_a::ext(self.token_a_addr.as_ref().unwrap().clone())
                    .with_static_gas(Gas(5 * TGAS))
                    .transfer(
                        &env::current_account_id(),
                        self.token_a_wallet.as_ref().unwrap(),
                        amount,
                    );
                self.token_a_balance += amount;
            }
            "B" => {
                token_b::ext(self.token_b_addr.as_ref().unwrap().clone())
                    .with_static_gas(Gas(5 * TGAS))
                    .transfer(
                        &env::current_account_id(),
                        self.token_b_wallet.as_ref().unwrap(),
                        amount,
                    );
                self.token_b_balance += amount;
            }
            _ => panic!("undefined token"),
        }
    }

    /// Deposit A and withdraw B
    pub fn token_a_to_b(&self, amount: Balance) {
        if !self.is_initialized() {
            log!("Not initialized");
            return;
        }

        let fee = self.fee("A", &amount);
        let withdraw = amount - fee;

        if withdraw > self.token_a_balance / 20 {
            log!("Only allow 5% token swap");
            return;
        }

        token_a::ext(self.token_a_addr.as_ref().unwrap().clone())
            .with_static_gas(Gas(5 * TGAS))
            .transfer(
                &env::current_account_id(),
                self.token_a_wallet.as_ref().unwrap(),
                amount,
            )
            .then(
                token_b::ext(self.token_b_addr.as_ref().unwrap().clone())
                    .with_static_gas(Gas(5 * TGAS))
                    .transfer(
                        self.token_b_wallet.as_ref().unwrap(),
                        &env::current_account_id(),
                        withdraw,
                    ),
            );
    }

    /// Deposit B and withdraw A
    pub fn token_b_to_a(&self, amount: Balance) {
        if !self.is_initialized() {
            log!("Not initialized");
            return;
        }

        let fee = self.fee("B", &amount);
        let withdraw = amount - fee;

        if withdraw > self.token_b_balance / 20 {
            log!("Only allow 5% token withdraw");
            return;
        }

        token_b::ext(self.token_b_addr.as_ref().unwrap().clone())
            .with_static_gas(Gas(5 * TGAS))
            .transfer(
                &env::current_account_id(),
                self.token_b_wallet.as_ref().unwrap(),
                amount,
            )
            .then(
                token_a::ext(self.token_a_addr.as_ref().unwrap().clone())
                    .with_static_gas(Gas(5 * TGAS))
                    .transfer(
                        self.token_a_wallet.as_ref().unwrap(),
                        &env::current_account_id(),
                        withdraw,
                    ),
            );
    }

    #[private]
    pub fn query_name_callback(&mut self, token: &str) {
        // Check if the promise succeeded by calling the method outlined in external.rs
        if !did_promise_succeed() {
            log!(format!("There was an error contacting for {}", token));
            return;
        }

        let name = match env::promise_result(0) {
            PromiseResult::Successful(value) => {
                Some(near_sdk::serde_json::from_slice::<String>(&value).unwrap())
            }
            _ => {
                log!(format!("There was an error contacting for {}", token));
                None
            }
        };

        match token {
            "A" => self.token_a_name = name,
            "B" => self.token_b_name = name,
            _ => panic!("undefined token"),
        }
    }

    #[private]
    pub fn create_wallet_callback(&mut self, token: &str) {
        // Check if the promise succeeded by calling the method outlined in external.rs
        if !did_promise_succeed() {
            log!(format!("There was an error contacting for {}", token));
            return;
        }

        let wallet = match env::promise_result(0) {
            PromiseResult::Successful(value) => {
                Some(near_sdk::serde_json::from_slice::<AccountId>(&value).unwrap())
            }
            _ => {
                log!(format!("There was an error contacting for {}", token));
                None
            }
        };

        match token {
            "A" => self.token_a_wallet = wallet,
            "B" => self.token_b_wallet = wallet,
            _ => panic!("undefined token"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn create_and_initialize_contract() {
        let mut contract = Contract::new(1);
        assert_eq!(contract.get_ratio(), 1);
        assert_eq!(contract.is_initialized(), false);
        let amm: AccountId = "amm".parse().unwrap();
        let token_a: AccountId = "token_a_addr".parse().unwrap();
        let token_b: AccountId = "token_b_addr".parse().unwrap();
        contract.initialization(amm, token_a, token_b);
    }
}
